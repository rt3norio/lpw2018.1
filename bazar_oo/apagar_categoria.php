<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 5/3/18
 * Time: 10:48 PM
 */
require_once( "./comum.php");
require_once( BASE_DIR . "/area_restrita.php");
require_once( BASE_DIR . "/classes/Categoria.php");

$id = filter_input( INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);

if (!isset($id)){
    exit;
}

Categoria::deleteCategory($id);
Header("location: categoria.php");