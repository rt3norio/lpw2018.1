<?php
	require_once( "./comum.php");
	require_once( BASE_DIR . "/classes/Categoria.php");
    require_once( BASE_DIR . "/area_restrita.php");


    $categorias = Categoria::findAll();
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo</title>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">
		<table border="1">
			<thead>
				<tr>
					<th>Código</th>
					<th>Descrição</th>
					<th>Taxa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach( $categorias as $categoria) {
				?>
				<tr>
					<td><?= $categoria->getIdCategoria() ?></td>
					<td><?= $categoria->getDescricao() ?></td>
					<td><?= $categoria->getTaxa() ?></td>
                    <td><form action="apagar_categoria.php" method="post" ><input type="hidden" value="<?=$categoria->getIdCategoria()?>" name="id"/><input type="submit" value="Apagar!"></form></td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>

	<?php require_once("rodape.inc"); ?>

</body>
</html>
