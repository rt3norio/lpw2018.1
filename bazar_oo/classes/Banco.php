<?php
abstract class Banco
{
  /**
  * Retorne um objeto PDO conectado
  */
  public static function obterConexao()
  {
    $pdo = new PDO('sqlite:bazar.sqlite3');
    #$pdo = new PDO('mysql:host=localhost:3306;dbname=bazar;charset=utf8mb4', 'root', 'vertrigo');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
  }
}
