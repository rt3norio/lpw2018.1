<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 5/3/18
 * Time: 10:52 PM
 */

session_start();
if( !isset( $_SESSION["usuario"] ) )
{
    Header("location: inicio.php");
    exit;
}