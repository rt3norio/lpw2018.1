<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php

for ($i=1; $i <= 100; $i++) { 
    if (($i%3 == 0 )&&($i%5 == 0)) {      //multiplo de 5
        echo "$i ";
        echo "<span class='sublinhado'>ploc</span>";
        echo '</br>';
    }
    elseif ($i%3 == 0 ) { //multiplo de 3
        echo "$i ";
        echo "<span class='negrito'>ping</span>";
        echo '</br>';
    }
    elseif ($i%5 == 0) { //multiplo de 3 e 5
        echo "$i ";
        echo "<span class='italico'>pong</span>";
        echo '</br>';
    }
    else {
        echo "$i ";
        echo "<span class='ok'>ok!</span>";
        echo '</br>';
    }
    
}

?>

</body>
</html>

<!--(i) quando o número for múltiplo de 3, imprima também "ping” em negrito ao lado, -->
<!--(ii) quando o número for múltiplo de 5, imprima “pong” em itálico ao lado do número, e -->
<!--(iii) se for múltiplo de ambos, coloque “ploc” sublinhado.-->