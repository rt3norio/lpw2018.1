<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php
if ((!isset($_GET['numero'])) | (!is_numeric($_GET['numero']))){
	$num = 100;
	}
else{
	$num = filter_input(INPUT_GET, 'numero', FILTER_SANITIZE_NUMBER_INT);
	}

function numero2palavra($numero){
    $palavraGerada = "";
    if (($numero%3 == 0 )&&($numero%5 == 0)) { //multiplo de 3 e 5
        $palavraGerada = "<span class='sublinhado'>ploc</span>";
    }
    elseif ($numero%3 == 0 ) { //multiplo de 3
        $palavraGerada = "<span class='negrito'>ping</span>";
    }
    elseif ($numero%5 == 0) { //multiplo de 5
        $palavraGerada = "<span class='italico'>pong</span>";
    }
    else {
        $palavraGerada = "<span class='ok'>ok!</span>";
    }
    return $palavraGerada;
}
for ($i=1; $i <= $num; $i++) { 
    echo "$i";
    echo Numero2Palavra($i);
    echo '</br>';
    
}
?>
</body>
</html>

<!--(i) quando o número for múltiplo de 3, imprima também "ping” em negrito ao lado, -->
<!--(ii) quando o número for múltiplo de 5, imprima “pong” em itálico ao lado do número, e -->
<!--(iii) se for múltiplo de ambos, coloque “ploc” sublinhado.-->
