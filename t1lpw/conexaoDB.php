<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/27/18
 * Time: 5:32 PM
 */

require 'Medoo.php';
use Medoo\Medoo;

$database = new Medoo([
    'database_type' => 'sqlite',
    'database_file' => 't1.db'
]);