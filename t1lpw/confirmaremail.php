<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/18/18
 * Time: 2:55 AM
 */

require 'Medoo.php';
use Medoo\Medoo;
$database = new Medoo([
    'database_type' => 'sqlite',
    'database_file' => 't1.db'
]);

function hash_esta_correto($hash,$email){
    $email_hash = hash('md5',$email);
    if($email_hash === $hash){
        return true;
    }
    else{
        return false;
    }
}

function retrieve_email_from_id($id,$database){
    $user = $database->select("user", [
        "email"
    ], [
        "id[=]" => $id
    ]);
    return $user[0]['email'];
}

##GET PARAMETERS
$id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
$hash = filter_input(INPUT_GET, "hash", FILTER_SANITIZE_STRING);


$email = retrieve_email_from_id($id,$database);

if(hash_esta_correto($hash,$email)){
    $database->update('user',[
        'verified' => 'Y'
    ],[
        'email[=]'=> $email
    ]);
    echo "email confirmado!";
}
else{
    echo "solicite outro email de confirmacao";
}





