<?php
require 'Medoo.php';
use Medoo\Medoo;

/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/18/18
 * Time: 1:12 AM
 */




$database = new Medoo([
    'database_type' => 'sqlite',
    'database_file' => 't1.db'
]);


$database->query("CREATE TABLE user (
                  id INTEGER PRIMARY KEY, 
                  email TEXT, 
                  password TEXT, 
                  profile TEXT, 
                  verified TEXT);");