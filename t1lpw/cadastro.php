<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/17/18
 * Time: 11:51 PM
 */

$profile = filter_input(INPUT_GET, "profile", FILTER_SANITIZE_STRING);

if (!($profile === "user")&!($profile === "provider")){ #redirects in case of wrong parameters
    echo "<script type='text/javascript'>location.href = 'erro.php?erro=perfil+incorreto';</script>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$profile?> registration</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</head>
<body>

<form class="form-horizontal" action="cadastrar.php" method="post">
    <fieldset>
        <input type="hidden" name="profile" id="profile" value="<?=$profile?>" />

        <!-- Form Name -->
        <legend><?=$profile?> registration</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">E-mail</label>
            <div class="col-md-4">
                <input id="email" name="email" type="email" placeholder="meu@email.com" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="ex.: 1234" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password2">Confirm Password</label>
            <div class="col-md-4">
                <input id="password2" name="password2" type="password" placeholder="O mesmo que acima" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </fieldset>
</form>



</body>
</html>
