<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/27/18
 * Time: 5:24 PM
 */

require_once "conexaoDB.php";

$servico_input = filter_input(INPUT_GET, "servico", FILTER_SANITIZE_STRING);
$subservico_input = filter_input(INPUT_GET, "subservico", FILTER_SANITIZE_STRING);


?>

<h1>Confirme o Preco do Servico</h1>

<table>
    <tr>
        <td>Tipo de Serviço: </td>
        <td><?=$servico_input?></td>
    </tr>
    <tr>
        <td>Sub Serviço: </td>
        <td><?=$subservico_input?></td>
    </tr>
    <tr>
        <td>Preço: </td>
        <td></td>
    </tr>
    <tr>
        <td>Seu Crédito: </td>
        <td></td>
    </tr>
    <tr>
        <td>Valor a Ser Cobrado: </td>
        <td></td>
    </tr>


</table>
