<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/27/18
 * Time: 3:39 PM
 */

require_once "conexaoDB.php";

$servicos = $database->select("servico", [
    "nome",
    "codigo"
], [

]);

$servico_input = filter_input(INPUT_GET, "servico", FILTER_SANITIZE_STRING);
$subservico_input = filter_input(INPUT_GET, "subservico", FILTER_SANITIZE_STRING);

if (isset($servico_input) && isset($subservico_input)){

        Header("location: confirmaServico.php?servico=$servico_input&subservico=$subservico_input");

}

if (isset($servico_input)){
    $subservicos = $database->select("subservico", [
        "nome",
        "codigo"
    ], [
        "servico[=]" => $servico_input
    ]);
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>

<h2>Solicitar Serviço</h2>
<p>Escolha em 2 passos. primeiro a categoria depois o serviço.</p>

<form>
    <select name="servico">
        <?php foreach ($servicos as $servico){?>
        <option value="<?=$servico["codigo"]?>" <?=($servico["codigo"] === $servico_input ? 'selected':'')?>  ><?=$servico["nome"]?></option>
        <?php }?>
    </select>

    <?php if (isset($servico_input)){
            echo"<br><br>";
        foreach ($subservicos as $subservico){
            echo "<input type='radio' name='subservico' value='".$subservico["codigo"]."'> ".$subservico["nome"]."<br>";
        }
    }?>
    <br>
    <input type="submit">




</form>

</body>
</html>

