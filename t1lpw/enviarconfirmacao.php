<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/18/18
 * Time: 2:26 AM
 */

require 'Medoo.php';
use Medoo\Medoo;


function send_email($id,$email){
    $hash = hash('md5',$email);
    $to      = $email;
    $subject = 'Email de Confirmação Serviço Fácil';
    $message = "Clique aqui para confirmar seu cadastro: <a href='http://rodrigotenorio.000webhostapp.com/t1lpw/confirmaremail.php?id=$id&hash=$hash'>Clique Aqui</a>";
    $headers = 'From: servico@facil.com' . "\r\n" .
        'Reply-To: servico@facil.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    mail($to, $subject, $message, $headers);

}


$database = new Medoo([
    'database_type' => 'sqlite',
    'database_file' => 't1.db'
]);

$id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);

$user = $database->select("user", [
    "email"
], [
    "id[=]" => $id
]);

$email = $user[0]['email'];

send_email($id,$email);

