<?php

require 'calculo.php';
include '../header.html';
$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$faixa = filter_input(INPUT_POST,'faixaEtaria',FILTER_SANITIZE_NUMBER_INT);
$doencaprevia = filter_input(INPUT_POST, 'doencaPrevia', FILTER_SANITIZE_STRING);


$preco = calcula_preco_plano(200,$faixa,$doencaprevia);

#echo 'valor do plano: '.$preco;
echo "<br><br>Sr. $nome, o Seu plano ficou calculado em $preco. Obrigado.";

?>

<br><br><br>
Regra: A faixa inicial comeca com R$ 200,
e a cada faixa encarece 50% sobre a anterior. 
Se tiver doenca prévia, acrescenta-se 30% sobre o valor da faixa.