<?php

function calcula_preco_plano($precoInicial,$faixa,$doencaprevia){
    $preco = $precoInicial;

    if($faixa >= 30){
        $preco += $preco * 0.5;
    }
    if($faixa >= 40){
        $preco += $preco * 0.5;
    }
    if($faixa >= 50){
        $preco += $preco * 0.5;
    }
    if($faixa >= 65){
        $preco += $preco * 0.5;
    }
    if($faixa >= 66){
        $preco += $preco * 0.5;
    }
    
    if ($doencaprevia == 's'){
        $preco  += $preco * 0.3;
    }
    return $preco;
}