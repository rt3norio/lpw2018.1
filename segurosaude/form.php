<?php

$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$faixa = filter_input(INPUT_POST,'faixaEtaria',FILTER_SANITIZE_NUMBER_INT);
$doencaprevia = filter_input(INPUT_POST, 'doencaPrevia', FILTER_SANITIZE_STRING);

$preco = 200;

if($faixa >= 30){
    $preco += $preco * 0.5;
}
if($faixa >= 40){
    $preco += $preco * 0.5;
}
if($faixa >= 50){
    $preco += $preco * 0.5;
}
if($faixa >= 65){
    $preco += $preco * 0.5;
}
if($faixa >= 66){
    $preco += $preco * 0.5;
}

if ($doencaprevia == 's'){
    $preco  += $preco * 0.3;
}

#echo 'valor do plano: '.$preco;
echo "<br><br>Sr. $nome, o Seu plano ficou calculado em $preco. Obrigado."

?>

<br><br><br>
Regra: A faixa inicial comeca com R$ 200,
e a cada faixa encarece 50% sobre a anterior. 
Se tiver doenca prévia, acrescenta-se 30% sobre o valor da faixa.