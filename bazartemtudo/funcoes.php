<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/26/18
 * Time: 4:50 PM
 */

function saudacao_por_hora(){
    date_default_timezone_set('America/Sao_Paulo');
    $hour = date('H', time());
    if ($hour <= 12){
        return "Bom dia!";
    }
    elseif ($hour <= 17){
        return "Boa tarde!";
    }
    else{
        return "Boa noite!";
    }
}