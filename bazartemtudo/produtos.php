<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 4/26/18
 * Time: 4:31 PM
 */
require_once "funcoes.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php require_once "cabecalho.html";?>

<h2><?= saudacao_por_hora()?> e seja bem vindo ao nosso bazar!</h2>
<h2>Confira abaixo a lista de produtos:</h2>
<table>
    <tr>
        <th>Produto</th>
        <th>Foto</th>
        <th>Preço</th>
    </tr>
    <?php for ($i=1;$i<6;$i++){?>
    <tr>
        <td>produto <?=$i?></td>
        <td>foto produto <?=$i?></td>
        <td>preco produto <?=$i?></td>
    </tr>

    <?php }?>
</table>

<?php require_once "rodape.html"?>
</body>
</html>